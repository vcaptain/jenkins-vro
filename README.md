Jenkins VRO package
===================

A package of vRealize Orchestrator workflows for publishing Jenkins builds as vRealize Automation XaaS catalog items. These can also be used from inside vRO in conjunctions with other workflows.

The package consists of 9 workflows, two actions and one configuration setting. 
